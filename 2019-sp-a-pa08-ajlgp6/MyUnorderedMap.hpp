void get_identity(std::string &my_id)
{
    my_id = "ajlgp6";
}

void get_virus_frequency(MyUnorderedMap<std::string, int> &in_tree)
{
    std::string uppercase;
    std::string original;
    std::string virus;
    unsigned int i;
    unsigned int end = 5;
    unsigned int start;
    while(std::getline(std::cin, uppercase))
    {
	uppercase.push_back('\r');
        original = uppercase;
        if(uppercase.length() > 4)
        {
            for(i = 0; i < uppercase.length(); i++)
            {
                if(islower(uppercase[i]))
                {
                    uppercase[i] = toupper(uppercase[i]);
                }
            }
            for(i = 0; i < uppercase.length()-5; i++)
            {
                if(uppercase.substr(i, end) == "VIRUS")
                {
                    start = i;
                    while(i+end < uppercase.length() && (isalnum(original[i+end]) || original[i+end] == '_'))
                    {
                        end++;
                    }
                    while(start > 0 && (isalnum(original[start-1]) || original[start-1] == '_'))
                    {
                        end++;
                        start--;
                    }
                    virus = original.substr(start, end);
                    if(in_tree.find(virus) == nullptr)
                    {
                        in_tree[virus] = 1;
                    }
                    else
                    {
                        in_tree[virus]++;
                    }
                }
                end = 5;
            }
        }
    }
}

//private
template <typename K, typename V>
int MyUnorderedMap<K, V>::hash(const K &key) const
{
    unsigned int sum = 0;
    for(unsigned int i = 0; i < key.length(); i++)
        sum+=(int)key[i];
    return sum%reserved_size;
}

//public
template <typename K, typename V>
MyUnorderedMap<K, V>::MyUnorderedMap()
{
    data_size = 0;
    reserved_size = 0;
    m_data = nullptr;
}

template <typename K, typename V>
MyUnorderedMap<K, V>::~MyUnorderedMap()
{
    delete[] m_data;
}

template <typename K, typename V>
MyUnorderedMap<K, V>::MyUnorderedMap(const MyUnorderedMap<K, V> &source)
{
    m_data = nullptr;
    data_size = source.data_size;
    reserved_size = source.reserved_size;
    m_data = new MyPair<K, V>[reserved_size];
    for(int i = 0; i < reserved_size; i++)
        m_data[i] = source.m_data[i];
}

template <typename K, typename V>
MyUnorderedMap<K, V> & MyUnorderedMap<K, V>::operator=(const MyUnorderedMap<K, V> &source)
{
    if(this != &source)
    {
        clear();
        m_data = new MyPair<K, V>[source.reserved_size];
        for(int i = 0; i < source.reserved_size; i++)
            m_data[i] = source.m_data[i];
    }
    data_size = source.data_size;
    reserved_size = source.reserved_size;
    return *this;
}

template <typename K, typename V>
V & MyUnorderedMap<K, V>::at(const K &key)
{
    if(data_size == 0)
        throw std::out_of_range("out_of_range::at()\n");
    int home = hash(key);
    int i = home+1;
    if(m_data[home].first == key)
        return m_data[home].second;
    if(i >= reserved_size)
        i = 0;
    while(m_data[i].first != "EMPTY" && i != home)
    {
        if(m_data[i].first == key)
            return m_data[i].second;
        i++;
        if(i == reserved_size)
            i = 0;
    }
    throw std::out_of_range("out_of_range::at()\n");
}

template <typename K, typename V>
V & MyUnorderedMap<K, V>::operator[](const K &key)
{
    if(reserved_size == 0)
        reserve(4);
    int home = hash(key);
    int i = home+1;
    if(m_data[home].first == key)
        return m_data[home].second;
    if(i >= reserved_size)
        i = 0;
    while(m_data[i].first != "EMPTY" && m_data[i].first != "TOMBSTONE")
    {
        if(m_data[i].first == key)
            return m_data[i].second;
        i++;
        if(i == reserved_size)
            i = 0;
    }
    m_data[i] = MyPair<K, V>(key);
    data_size++;
    if(float(data_size)/reserved_size >= .6){
        reserve(reserved_size*2);
    }
    return find(key)->second;
}

template <typename K, typename V>
bool MyUnorderedMap<K, V>::empty() const
{
    return !data_size;
}

template <typename K, typename V>
int MyUnorderedMap<K, V>::size() const
{
    return data_size;
}

template <typename K, typename V>
void MyUnorderedMap<K, V>::clear()
{
    delete[] m_data;
    data_size = 0;
    reserved_size = 0;
    m_data = nullptr;
}

template <typename K, typename V>
void MyUnorderedMap<K, V>::insert(const MyPair<K, V> &init_pair)
{
    if(reserved_size == 0)
        reserve(4);
    int home = hash(init_pair.first);
    int i = home;
    while(m_data[i].first != "EMPTY" && m_data[i].first != "TOMBSTONE")
    {
        if(m_data[i].first == init_pair.first)
            m_data[i].second = init_pair.second;
        i++;
        if(i == reserved_size)
            i = 0;
    }
    m_data[i] = init_pair;
    data_size++;
    if(float(data_size)/reserved_size >= .6)
        reserve(reserved_size*2);
}

template <typename K, typename V>
void MyUnorderedMap<K, V>::erase(const K &key)
{
    if(reserved_size == 0)
        return;
    int home = hash(key);
    int i = home+1;
    if(i >= reserved_size)
        i = 0;
    if(m_data[home].first == key){
        m_data[home].first = "TOMBSTONE";
        data_size--;
        return;
    }
    while(m_data[i].first != "EMPTY" && m_data[i].first != "TOMBSTONE" && i != home)
    {
        if(m_data[i].first == key){
            data_size--;
            m_data[i].first == "TOMBSTONE";
        }
        i++;
        if(i == reserved_size)
            i = 0;
    }

    return;
}

template <typename K, typename V>
MyPair<K, V> * MyUnorderedMap<K, V>::find(const K &key) const
{
    if(data_size == 0)
        return nullptr;
    int home = hash(key);
    int i = home+1;
    if(m_data[home].first == key)
    {
        return &(m_data[home]);
    }
    if(i >= reserved_size)
        i = 0;
    while(m_data[i].first != "EMPTY" && i != home)
    {
        if(m_data[i].first == key)
            return &(m_data[i]);
        i++;
        if(i == reserved_size)
            i = 0;
    }
    return nullptr;
}

template <typename K, typename V>
void MyUnorderedMap<K, V>::print() const
{
    int total = 0;
    if(data_size != 0)
    {
        cout<<"(";
        for(int i = 0; i < reserved_size; i++)
        {
            if(m_data[i].first != "EMPTY" && m_data[i].first != "TOMBSTONE")
            {
                total++;
                cout<<"["<<m_data[i].first<<"]="<<m_data[i].second;
                if(total != data_size)
                    cout<<","<<endl<<" ";
                else
                    cout<<")";
            }
        }
    }
}

template <typename K, typename V>
int MyUnorderedMap<K, V>::count(const K &key) const
{
    if(data_size == 0)
        return 0;
    int home = hash(key);
    int i = home+1;
    if(m_data[home].first == key)
        return 1;
    if(i >= reserved_size)
        i = 0;
    while(m_data[i].first != "EMPTY" && i != home)
    {
        if(m_data[i].first == key)
            return 1;
        i++;
        if(i == reserved_size)
            i = 0;
    }
    return 0;
}

template <typename K, typename V>
void MyUnorderedMap<K, V>::reserve(int new_cap)
{
    if(data_size == 0)
    {
        reserved_size = new_cap;
        delete[] m_data;
        m_data = new MyPair<K, V>[new_cap];
        for(int i = 0; i < reserved_size; i++)
            m_data[i] = MyPair<K,V>("EMPTY");
    }
    else
    {
        MyPair<K, V> * temp = new MyPair<K, V>[new_cap];
        for(int i = 0; i < new_cap; i++)
            temp[i] = MyPair<K,V>("EMPTY");
        for(int i = 0; i < reserved_size; i++)
            temp[i] = m_data[i];
        delete[] m_data;
        reserved_size = new_cap;
        data_size = 0;
        m_data = new MyPair<K, V>[new_cap];
        for(int i = 0; i < reserved_size; i++)
            m_data[i] = MyPair<K,V>("EMPTY");
        for(int i = 0; i < reserved_size; i++)
            if(temp[i].first != "EMPTY" && temp[i].first != "TOMBSTONE")
                insert(temp[i]);
        delete[] temp;
    }
}
