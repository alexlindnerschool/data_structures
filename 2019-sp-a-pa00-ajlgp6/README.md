# Assignment 0
Thoroughly read the syllabus sections on "Programming assignments" and "Grading", including the links referenced within. 
These sections give good tips, tricks, hints, and instructions for programming assignments, including how to submit via Git: https://web.mst.edu/~taylorpat/Courses_files/DataStructures/Syllabus.html

## Virtual Machine (VM)
Download the virtual machine specified on the course website, and follow the instructions on the website for configurig it.

## Git
From within the virtual machine, open a terminal and clone this repository using the bash command line.
The resulting folder is your repository.
Use the `cd` command to enter the directory. 
This is where you should do and store your work.

## Basic Bash
From within the virtual machine, read and perform the commands in the following guide on how to manage basic Linux/Unix command line work: http://linuxcommand.org/lc3_learning_the_shell.php .
Add the ~/.bash_history file (which shows your command history from the tutorial) into your repository; leave it named .bash_history (do not rename it).
Note: it is a hidden file (there is a . preceding it), so might not display as you expect!
Commit your changes.

## Your C++ program
From within the virtual machine, add a file titled pa00.cpp to the repository.
It should take user input via cin, which it outputs via cout after "Hello ", followed by a newline.
Thus, when run, it should ask the user for input (type "world), and then it should output "Hello world" with one newline at the end.
If you ran it and typed "Name", it would output "Hello Name".
Note: the filename and EXACT text should be used. 

## Testing
To test if your program works, do the following:

### diff
In the virtual machine, run at the bash command line:
```sh
$ cat sample_input.txt
$ cat sample_output.txt

$ g++ -g pa00.cpp
$ ./a.out <"sample_input.txt" >"my_output.txt"

$ diff --color "sample_output.txt" "my_output.txt"
$ diff -y --color "sample_output.txt" "my_output.txt"

$ vim -d "sample_output.txt" "my_output.txt"
# To exit vim, type esc followed by :q
```
Take a screenshot of the result of these commands, and name it diff.png.

### Debuggers: gdb and cgdb
After installing cgdb, type the following:
```sh
$ g++ -g pa00.cpp
$ cgdb a.out
(gdb) start <sample_input.txt
(gdb) next
(gdb) next
(gdb) next
(gdb) quit
```
Take a screenshot while you are stil in cgdb (before quitting), and name it debug.png.

## Git: add, commit, push
From within the virtual machine, add, commit, and push all the non-generated files. 
This means add your cpp and png files, but not a.out, etc.
Verify you can see the files on git-classes in the web interface.
If you can see the correct files on git-classes in your master branch, your submission is complete.

## Double check everything
Re-read the section of the syllabus titled: "Things you should check before you submit"

