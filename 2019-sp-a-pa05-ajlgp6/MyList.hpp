void get_identity(string &my_id)
{
    my_id = "ajlgp6";
}

template <typename T>
MyList<T>::MyList()
{
    m_size = 0;
    m_sentinel = new Node<T>(nullptr, nullptr);
    m_sentinel->m_next = m_sentinel;
    m_sentinel->m_prev = m_sentinel;
}

template <typename T>
MyList<T>::~MyList()
{
    m_size = 0;
    Node<T> *remove = m_sentinel->m_next;
    while(remove != m_sentinel)
    {
        Node<T> *place = remove;
        remove = remove->m_next;
        delete place;
    }
    m_sentinel->m_next = m_sentinel;
    m_sentinel->m_prev = m_sentinel;
    delete m_sentinel;
}

template <typename T>
MyList<T> & MyList<T>::operator=(const MyList<T> &source)
{
    m_size = 0;
    Node<T> *remove = m_sentinel->m_next;
    while(remove != m_sentinel)
    {
        Node<T> *place = remove;
        remove = remove->m_next;
        delete place;
    }
    m_sentinel->m_next = m_sentinel;
    m_sentinel->m_prev = m_sentinel;
    Node<T> *place = source.m_sentinel->m_next;
    for(int i = 0; i < source.m_size; i++)
    {
        push_back(place->m_element);
        place = place->m_next;
    }
    return *this;
}

template <typename T>
MyList<T>::MyList(const MyList<T> &source)
{
    m_size = 0;
    m_sentinel = new Node<T>(nullptr, nullptr);
    m_sentinel->m_next = m_sentinel;
    m_sentinel->m_prev = m_sentinel;
    
    Node<T> *place = source.m_sentinel->m_next;
    for(int k = 0; k < source.m_size; k++)
    {
        push_back(place->m_element);
        place = place->m_next;
    }
}

template <typename T>
T & MyList<T>::front()
{
    return m_sentinel->m_next->m_element;
}

template <typename T>
T & MyList<T>::back()
{
    return m_sentinel->m_prev->m_element;
}

template <typename T>
void MyList<T>::assign(int count, const T &value)
{
    if(m_size != 0)
    {
        m_size = 0;
        Node<T> *remove = m_sentinel->m_next;
        while(remove != m_sentinel)
        {
            Node<T> *place = remove;
            remove = remove->m_next;
            delete place;
        }
        m_sentinel->m_next = m_sentinel;
        m_sentinel->m_prev = m_sentinel;
    }
    for(int k = 0; k < count; k++)
    {
        this->push_back(value);
    }
}

template <typename T>
void MyList<T>::clear()
{
    m_size = 0;
    Node<T> *remove = m_sentinel->m_next;
    while(remove != m_sentinel)
    {
        Node<T> *place = remove;
        remove = remove->m_next;
        delete place;
    }
    m_sentinel->m_next = m_sentinel;
    m_sentinel->m_prev = m_sentinel;

}

template <typename T>
void MyList<T>::push_front(const T &x)
{
    m_size++;
    m_sentinel->m_next = new Node<T>(x, m_sentinel, m_sentinel->m_next);
    m_sentinel->m_next->m_next->m_prev = m_sentinel->m_next;
}

template <typename T>
void MyList<T>::push_back(const T &x)
{
    m_size++;
    m_sentinel->m_prev = new Node<T>(x, m_sentinel->m_prev, m_sentinel);
    m_sentinel->m_prev->m_prev->m_next = m_sentinel->m_prev;
}

template <typename T>
void MyList<T>::pop_back()
{
    if(m_size > 0)
    {
	m_size--;
        Node<T> *remove = m_sentinel->m_prev;
        m_sentinel->m_prev = m_sentinel->m_prev->m_prev;
        m_sentinel->m_prev->m_next = m_sentinel;
        delete remove;
    }
}

template <typename T>
void MyList<T>::pop_front()
{
    if(m_size > 0)
    {
    	m_size--;
        Node<T> *remove = m_sentinel->m_next;
        m_sentinel->m_next = m_sentinel->m_next->m_next;
        m_sentinel->m_next->m_prev = m_sentinel;
        delete remove;
    }
}

template <typename T>
void MyList<T>::insert(int i, const T &x)
{
    if(i <= m_size && i >= 0)
    {
    	m_size++;
        Node<T> *place = m_sentinel;
        for(int j = 0; j < i; j++)
        {
            place = place->m_next;
        }
        place->m_next = new Node<T>(x , place, place->m_next);
        place->m_next->m_next->m_prev = place->m_next;
    }
}

template <typename T>
void MyList<T>::remove(T value)
{
    Node<T> *place = m_sentinel;
    for(int k = 0; k <= m_size; k++)
    {
        if(place->m_next->m_element == value)
        {
            Node<T> *remove = place->m_next;
            place->m_next = place->m_next->m_next;
            place->m_next->m_prev = place;
            m_size--;
            delete remove;
        }
        if(place->m_next->m_element != value)
            place = place->m_next;
    }
}

template <typename T>
void MyList<T>::erase(int i)
{
    if(i >= 0 && i < m_size)
    {
        Node<T> *curr = m_sentinel;
        for(int j = 0; j < i; j++)
            curr = curr->m_next;
        Node<T> *temp = curr->m_next;
        curr->m_next = curr->m_next->m_next;
        curr->m_next->m_prev = curr;
        m_size--;
        delete temp;
    }
}

template <typename T>
void MyList<T>::reverse()
{
    MyList<T> copylist = *this;
    clear();
    while(copylist.size() !=  0){
        push_back(copylist.back());
        copylist.pop_back();
    }
}

template <typename T>
bool MyList<T>::empty()
{
    return !m_size;
}

template <typename T>
int MyList<T>::size()
{
    return m_size;
}
