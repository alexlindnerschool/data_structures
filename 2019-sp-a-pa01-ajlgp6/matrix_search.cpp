/**
Here in the .cpp you should define and implement the functions that are declared in the .h file,
and optionally any you declared, but not defined, above main.
**/

#include "matrix_search.h"

void get_identity(string &my_id)
{
    my_id = "ajlgp6";
}

char ** build_matrix(int rows, int cols)
{
    char **matrix = new char *[rows];
    for(int i = 0; i < rows; i++)
        matrix[i] = new char[cols];
    return matrix;
}

void fill_matrix(int rows, int cols, char **matrix)
{
    for(int i = 0; i < rows; i++)
        for(int j = 0; j < cols; j++)
            cin>>matrix[i][j];
}

void print_matrix(int rows, int cols, char **matrix)
{
    cout<<matrix[rows][cols];
}

void delete_matrix(int rows, char **matrix)
{
    for(int i = 0; i < rows; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
}

void matrix_search(int sol[], string word, int rows, int cols, char **matrix)
{
    int size = word.length();
    bool wordFound = false;
    for(int i = 0; i < rows && !wordFound; i++) {
        for(int j = 0; j < cols && !wordFound; j++) {
            if(word[0] == matrix[i][j]) {
                sol[0] = i;
                sol[1] = j;
                if(i+1 >= size) {
                    //subtract from i (up)
                    for(int k = 1; k < size && word[k] == matrix[i-k][j]; k++) {
                        if(k == size-1 && word[k] == matrix[i-k][j]) {
                            sol[2] = i-k, sol[3] = j;
                            wordFound = true;
                        }
                    }
                    if(j+1 >= size && !wordFound){
                        //and subtract from j (up left)
                        for(int k = 1; k < size && word[k] == matrix[i-k][j-k]; k++) {
                            if(k == size-1 && word[k] == matrix[i-k][j-k]) {
                                sol[2] = i-k, sol[3] = j-k;
                                wordFound = true;
                            }
                        }
                    }
                    if(cols-(j) >= size && !wordFound) {
                        //and add to j (up right)
                        for(int k = 1; k < size && word[k] == matrix[i-k][j+k]; k++) {
                            if(k == size-1 && word[k] == matrix[i-k][j+k]) {
                                sol[2] = i-k, sol[3] = j+k;
                                wordFound = true;
                            }
                        }
                    }
                }
                if(j+1 >= size && !wordFound) {
                    //subtract from j (left)
                    for(int k = 1; k < size && word[k] == matrix[i][j-k]; k++) {
                        if(k == size-1 && word[k] == matrix[i][j-k]) {
                            sol[2] = i, sol[3] = j-k;
                            wordFound = true;
                        }
                    }
                }
                if(rows-(i) >= size && !wordFound) {
                    //add to i (down)
                    for(int k = 1; k < size && word[k] == matrix[i+k][j]; k++) {
                        if(k == size-1 && word[k] == matrix[i+k][j]) {
                            sol[2] = i+k, sol[3] = j;
                            wordFound = true;
                        }
                    }
                    if(j+1 >= size && !wordFound) {
                        //and subtract from j (down left)
                        for(int k = 1; k < size && word[k] == matrix[i+k][j-k]; k++) {
                            if(k == size-1 && word[k] == matrix[i+k][j-k]) {
                                sol[2] = i+k, sol[3] = j-k;
                                wordFound = true;
                            }
                        }
                    }
                    if(cols-(j) >= size && !wordFound) {
                        //and add to j (down right)
                        for(int k = 1; k < size && word[k] == matrix[i+k][j+k]; k++) {
                            if(k == size-1 && word[k] == matrix[i+k][j+k]) {
                                sol[2] = i+k, sol[3] = j+k;
                                wordFound = true;
                            }
                        }
                    }
                }
                if(cols-(j) >= size && !wordFound) {
                    //add to j (right)
                    for(int k = 1; k < size && word[k] == matrix[i][j+k]; k++) {
                        if(k == size-1 && word[k] == matrix[i][j+k]) {
                            sol[2] = i, sol[3] = j+k;
                            wordFound = true;
                        }
                    }
                }
            }
        }
    }
    if(!wordFound){
        for(int i = 0; i < 4; i++)
            sol[i] = -1;
        cout<<"The pattern was not found.";
    }
    else
        cout<<"Start pos:("<<sol[0]<<", "<<sol[1]<<") to End pos:("<<sol[2]<<", "<<sol[3]<<")";
    cout<<endl<<endl;
}
