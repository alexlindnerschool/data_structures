/**
Implement the main here to match input and output.
If you want to add extra functions, you can do so above main.
**/

#include "matrix_search.h"

int main()
{
    int rows;
    int cols;
    int numMatrix;
    string word;

    cin>>numMatrix;
    for(int i = 0; i < numMatrix; i++) {
        cin>>rows;
        cin>>cols;

        char **matrix = build_matrix(rows, cols);
        fill_matrix(rows, cols, matrix);
        /*for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                print_matrix(i, j, matrix);
                cout<<" ";
            }
            cout<<endl;
        }*/
        int sol[4];
        cin>>word;
        cout<<"Searching for \""<<word<<"\" in matrix "<<i<<" yields:"<<endl;
        matrix_search(sol, word, rows, cols, matrix);
        delete_matrix(rows,matrix);
    }
    return 0;
}

