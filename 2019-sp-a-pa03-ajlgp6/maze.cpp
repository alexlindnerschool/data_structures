/* Here in the .cpp you should define and implement everything declared in the .h file.
 */

#include "maze.h"

void get_identity(string &my_id)
{
    my_id = "ajlgp6";
}


string * build_matrix(int rows)
{
    string *matrix;
    matrix = new string[rows];
    return matrix;
}


void fill_matrix(string *matrix, int rows)
{
    for(int i = 0; i < rows; i++)
        std::getline(cin,matrix[i]);
}


void print_matrix(string *matrix, int rows)
{
    for(int i = 0; i < rows; i++)
        cout<<matrix[i]<<endl;
}

void delete_matrix(string *&matrix)
{
    delete[] matrix;
    matrix = nullptr;
}


void find_start(string *matrix, int rows, int &row, int &col)
{
    for(int i = 0; i < rows; i++)
        for(unsigned int j = 0; j < matrix[0].length(); j++)
            if(matrix[i][j] == 'N') {
                row = i;
                col = j;
            }
}

bool find_exit(string *matrix, int row, int col)
{
    static string dir[4] = {"North","South","East","West"};
    if(at_end(matrix,row,col))
        return true;
    for(int i = 0; i < 4; i++) {
        if(valid_move(matrix,row,col,dir[i])) {
            if(matrix[row][col] != 'N')
                matrix[row][col] = '@';
            if(i == 0) row--;
            else if(i == 1) row++;
            else if(i == 2) col++;
            else if(i == 3) col--;
            if(find_exit(matrix,row,col))
                return true;
            if(i == 0) row++;
            else if(i == 1) row--;
            else if(i == 2) col--;
            else if(i == 3) col++;
            if(matrix[row][col] != 'N')
                matrix[row][col] = ' ';
        }
    }
    return false;
}


bool at_end(string *matrix, int row, int col)
{
    if(matrix[row][col] == 'E')
        return true;
    return false;
}

bool valid_move(string *matrix, int row, int col, string direction)
{
    if(direction == "North") {
        if((matrix[row-1][col] == ' ') || (matrix[row-1][col] == 'E'))
            return true;
    }
    else if(direction == "South") {
        if((matrix[row+1][col] == ' ') || (matrix[row+1][col] == 'E'))
            return true;
    }
    else if(direction == "East") {
        if((matrix[row][col+1] == ' ') || (matrix[row][col+1] == 'E'))
            return true;
    }
    else if(direction == "West") {
        if((matrix[row][col-1] == ' ') || (matrix[row][col-1] == 'E'))
            return true;
    }
    return false;
}

