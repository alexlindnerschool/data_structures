/* Declare any non-required functions above main.
 * The duty of main here is to interact with the user, take in input, and manage wrapping output and runtime.
 * Remember, if you are considering putting something in main or a function, double check the specifications.
 * Each function should only do what it is specified to do, and no more.
 */

#include "maze.h"

int main()
{
    string line;
    int rows, row, col, map = 0;
    cin>>rows;
    while(rows != 0) {
        string *matrix = build_matrix(rows);
        std::getline(cin,line);
        fill_matrix(matrix,rows);
        find_start(matrix,rows,row,col);
        if(find_exit(matrix,row,col))
            cout<<"Map "<<map<<" -- Solution found:"<<endl;
        else
            cout<<"Map "<<map<<" -- No solution found:"<<endl;
        print_matrix(matrix,rows);
        delete_matrix(matrix);
        cout<<endl;
        cin>>rows;
        map++;
    }
    return 0;
}

