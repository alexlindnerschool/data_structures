// Mimic the std:: version

template <typename T>
void my_sort(T *array, int size)
{
    MyHeap<T> temp(array, size);
    for(int i = 0; i < size; i++){
        array[i] = temp.top();
        temp.pop();
    }
}
