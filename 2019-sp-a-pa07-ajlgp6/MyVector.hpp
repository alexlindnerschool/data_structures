/* Define all your MyVector-related functions here.
 * Also, define the swap function here.
 * You do not need to include the h file.
 * It included this file at the bottom.
 */

void get_identity(string &my_id)
{
    my_id = "ajlgp6";
}

template <typename T>
void swap(T &a, T &b)
{
    T temp = a;
    a = b;
    b = temp;
}

template <typename T>
MyVector<T>::MyVector()
{
    m_data = nullptr;
    reserved_size = 0;
    data_size = 0;
}

template <typename T>
MyVector<T>::~MyVector()
{
    delete[] m_data;
}

template <typename T>
MyVector<T> & MyVector<T>::operator=(const MyVector<T> &source)
{
    m_data = nullptr;
    data_size = source.data_size;
    reserved_size = source.reserved_size;
    m_data = new T[data_size];

    for(int i = 0; i < data_size; i++)
        m_data[i] = source.m_data[i];
    return *this;
}

template <typename T>
MyVector<T>::MyVector(const MyVector<T> &source)
{
    m_data = nullptr;
    data_size = source.data_size;
    reserved_size = source.reserved_size;
    m_data = new T[data_size];

    for(int i = 0; i < data_size; i++)
        m_data[i] = source.m_data[i];
}

template <typename T>
T & MyVector<T>::operator[](int i)
{
    return m_data[i];
}

template <typename T>
T & MyVector<T>::at(int index)
{
    if(data_size != 0 && (index < 0 || index >= data_size))
        throw std::out_of_range("Index out of Range");
    return m_data[index];
}

template <typename T>
T & MyVector<T>::front()
{
    return this->at(0);
}

template <typename T>
T & MyVector<T>::back()
{
    return this->at(data_size-1);
}

template <typename T>
int MyVector<T>::capacity()
{
    return reserved_size;
}

template <typename T>
void MyVector<T>::reserve(int new_cap)
{
    if(new_cap > reserved_size){
        if(reserved_size == 0){
            m_data = new T[new_cap];
            reserved_size = new_cap;
        }
        else{
            T * temp = new T[new_cap];
            for(int i = 0; i < data_size; i++)
                temp[i] = m_data[i];
            reserved_size = new_cap;
            delete[] m_data;
            m_data = temp;
        }
    }
}

template <typename T>
void MyVector<T>::shrink_to_fit()
{
    T * temp = new T[data_size*2];
    for(int i = 0; i < data_size; i++)
        temp[i] = m_data[i];
    reserved_size = data_size*2;
    delete[] m_data;
    m_data = temp;
}

template <typename T>
void MyVector<T>::assign(int count, const T &value)
{
    m_data = nullptr;

    reserved_size = count;
    data_size = count;
    m_data = new T[data_size];

    for(int i = 0; i < count; i++)
        m_data[i] = value;
}

template <typename T>
void MyVector<T>::clear()
{
    m_data = nullptr;
    data_size = 0;
    reserved_size = 0;
}

template <typename T>
void MyVector<T>::push_back(const T &x)
{
    if(data_size >= reserved_size){
        if(reserved_size == 0)
            reserve(1);
        else
            reserve(reserved_size*2);
    }
    this->at(data_size++) = x;
}

template <typename T>
void MyVector<T>::pop_back()
{
    data_size--;
    if(data_size == 0)
        m_data = nullptr;
    if(data_size == 0 || data_size < reserved_size/4)
        this->shrink_to_fit();
}

template <typename T>
void MyVector<T>::insert(int i, const T &x)
{
    if(reserved_size == 0)
        reserve(1);
    if(i < 0 || i > data_size)
        throw std::out_of_range("Index out of Range");
    else{
        if(data_size+1 >= reserved_size)
            reserve(reserved_size*2);
        this->at(data_size++) = x;
        for(int j = data_size-1; j > i; j--)
            swap(this->at(j), this->at(j-1));
    }
}

template <typename T>
void MyVector<T>::erase(int i)
{
    if(i >= 0 && i < data_size){
        if(i == data_size-1)
            data_size--;
        else{
            for(int j = i+1; j < data_size; j++)
                swap(this->at(j-1),this->at(j));
            data_size--;
        }
        if(data_size < reserved_size/4)
            this->shrink_to_fit();
    }
    else
	throw std::out_of_range("Index out of Range");
}

template <typename T>
int MyVector<T>::size()
{
    return data_size;
}
