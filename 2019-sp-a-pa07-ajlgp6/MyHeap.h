// You don't need to have a MyHeap.hpp, but you could if you want
// Declare, and/or implement your heap class here. 
// If you are having a hard time figuring out how to start, look at the past assignments.
#include "MyVector.h"

template <typename T>
void get_identity(std::string &my_id)
{
    my_id = "ajlgp6";
}

template <typename E>
class MyHeap
{
    private:
        MyVector<E> heap;
        int maxsize = 0;
        int n = 0;

    public:
        MyHeap()
        {
            maxsize = 0;
            n = 0;
        }

        MyHeap(E *h, int size)
        {
            n = size;
            maxsize = size;
            for(int i = 0; i < size; i++)
                heap.push_back(h[i]);
            buildHeap();
        }
        template <typename T>
        MyHeap(const MyHeap<E> &source)
        {
            MyVector<T> temp(source);
            n = source.size();
            maxsize = source.size();
            for(int i = 0; i < n; i++){
                heap[i] = temp[i];
            }
        }
        template <typename T>
        MyHeap<E> & operator=(const MyHeap<E> &source)
        {
            if(this == &source)
                return *this;
            MyVector<T> temp(source);
            n = source.size();
            maxsize = source.size();
            for(int i = 0; i < n; i++){
                heap[i] = temp[i];
            }
            return *this;
        }

        ~MyHeap(){}

        E & top()
        {
            return heap.front();
        }

        void pop()
        {
            n--;
            if(n <= maxsize/4)
                heap.shrink_to_fit();
            heap.erase(0);
            buildHeap();
        }

        void push(const E &it)
        {
            if(n >= maxsize)
                heap.reserve(maxsize*2);
            heap.insert(n, it);
            n++;
            buildHeap();
        }

        bool empty() const
        {
            if(n == 0)
                return true;
            return false;
        }

        int size() const
        {
            return n;
        }

        int leftchild(int pos) const
        {
            return ((2 * pos) + 1);
        }

        int rightchild(int pos) const
        {
            return ((2 * pos) + 2);
        }

        int parent(int pos) const
        {
            return ((pos - 1) / 2);
        }

        void siftdown(int i)
        {
            int l = leftchild(i);
            int r = rightchild(i);
            int largest = i;

            if(l < n && heap[l] > heap[i])
                largest = l;

            if(r < n && heap[r] > heap[largest])
                largest = r;

            if(largest != i)
            {
                std::swap(heap[i], heap[largest]);
                siftdown(largest);
            }
        }

        void buildHeap()
        {
            for(int i = parent(n - 1); i >= 0; i--)
            {
                siftdown(i);
            }
        }
};
