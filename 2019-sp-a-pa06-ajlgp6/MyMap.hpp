void get_identity(std::string &my_id)
{
    my_id = "ajlgp6";
}

void get_letter_frequency(MyMap<char, int> &in_tree)
{
    char letter;
    while(cin.get(letter))
    {
        if(letter != '\n')
        {
            if(in_tree.find(letter) != nullptr)
            {
                in_tree.at(letter)++;
            }
            else
            {
                in_tree.insert(MyPair<char, int>(letter, 1));
            }
        }
    }
}

//private
template <typename K, typename V>
V & MyMap<K, V>::at_helper(TreeNode<MyPair<K, V>> *&rt, const K &key)
{
    if(rt == nullptr)
        throw std::out_of_range("MyMap::at\n");
    else if(rt->data.first < key)
        return at_helper(rt->right, key);
    else if(rt->data.first > key)
        return at_helper(rt->left, key);
    else
        return rt->data.second;
}
template <typename K, typename V>
int MyMap<K, V>::size_helper(TreeNode<MyPair<K, V>> *rt) const
{
    if(rt == nullptr)
        return 0;
    return 1 + size_helper(rt->right) + size_helper(rt->left);
}
template <typename K, typename V>
void MyMap<K, V>::clear_helper(TreeNode<MyPair<K, V>> *&rt)
{
    if(rt == nullptr)
        return;
    clear_helper(rt->right);
    clear_helper(rt->left);
    delete rt;
}
template <typename K, typename V>
void MyMap<K, V>::insert_helper(TreeNode<MyPair<K, V>> *&rt, const MyPair<K, V> &init_pair)
{
    if(rt == nullptr)
    {
        rt = new TreeNode<MyPair<K, V>>(init_pair, nullptr, nullptr);
    }
    else if(init_pair.first < rt->data.first)
    {
        insert_helper(rt->left, init_pair);
    }
    else
    {
        insert_helper(rt->right, init_pair);
    }
}
template <typename K, typename V>
TreeNode<MyPair<K, V>> * MyMap<K, V>::get_min(TreeNode<MyPair<K, V>> *rt)
{
    if(rt->left == nullptr)
    {
        return rt;
    }
    else
    {
        return get_min(rt->left);
    }
}
template <typename K, typename V>
void MyMap<K, V>::erase_helper(TreeNode<MyPair<K, V>> *&rt, const K &key)
{
    if(rt == nullptr)
    {
        return;
    }
    else if(rt->data.first < key)
    {
        erase_helper(rt->right, key);
    }
    else if(rt->data.first > key)
    {
        erase_helper(rt->left, key);
    }
    else
    {
        TreeNode<MyPair<K,V>> *change = rt;

        if(rt->left == nullptr)
        {
            rt = rt->right;
            delete change;
        }
        else if(rt->right == nullptr)
        {
            rt = rt->left;
            delete change;
        }
        else
        {
            TreeNode<MyPair<K,V>> *change2 = get_min(rt->right);
            rt->data.second = change2->data.second;
            rt->data.first = change2->data.first;
            erase_helper(rt->right, change2->data.first);

        }
    }
}
template <typename K, typename V>
MyPair<K, V> * MyMap<K, V>::find_helper(TreeNode<MyPair<K, V>> *rt, const K &key) const
{
    if(rt == nullptr)
    {
        return nullptr;
    }
    else if(rt->data.first > key)
    {
        return find_helper(rt->left, key);
    }
    else if(rt->data.first < key)
    {
        return find_helper(rt->right, key);
    }
    else
    {
        return &rt->data;
    }
}
template <typename K, typename V>
void MyMap<K, V>::print_helper(TreeNode<MyPair<K, V>> *rt, std::string indent) const
{
    if(rt == nullptr)
    {
        cout<<indent<<"   [empty]"<<endl;
        return;
    }
    print_helper(rt->right, indent+="  ");
    cout<<indent<<" ["<<rt->data.first<<"]="<<rt->data.second<<endl;
    print_helper(rt->left, indent);
}
template <typename K, typename V>
int MyMap<K, V>::count_helper(TreeNode<MyPair<K, V>> *rt, const K &key) const
{
    if(rt == nullptr)
    {
        return 0;
    }
    else if(rt->data.first == key)
    {
        return 1;
    }
    else if(rt->data.first > key)
    {
        count_helper(rt->left, key);
    }
    else if(rt->data.first < key)
    {
        count_helper(rt->right, key);
    }
}
template <typename K, typename V>
TreeNode<MyPair<K, V>> * MyMap<K, V>::clone(const TreeNode<MyPair<K, V>> *rt)
{
    if(rt != nullptr)
    {
        TreeNode<MyPair<K,V>> *head = new TreeNode<MyPair<K,V>>(rt->data, clone(rt->left), clone(rt->right));
        return head;
    }
    else
    {
        return nullptr;
    }
}

//public
template <typename K, typename V>
MyMap<K, V>::MyMap()
{
    
}
template <typename K, typename V>
MyMap<K, V>::~MyMap()
{
    clear();
}
template <typename K, typename V>
MyMap<K, V>::MyMap(const MyMap<K, V> & source)
{
    root = clone(source.root);   
}

template <typename K, typename V>
MyMap<K, V> & MyMap<K,V>::operator=(const MyMap<K, V> &source)
{
    if(this != &source)
    {
        clear();
        root = clone(source.root);
    }
    return *this;
}

template <typename K, typename V>
V & MyMap<K, V>::at(const K &key)
{
    return at_helper(root, key);
}
template <typename K, typename V>
V & MyMap<K, V>::operator[](const K &key)
{
    if(find_helper(root, key) == nullptr)
        insert_helper(root, MyPair<K, V>(key));
    return at_helper(root, key);
}
template <typename K, typename V>
bool MyMap<K, V>::empty() const
{
    return (root == nullptr);
}
template <typename K, typename V>
int MyMap<K, V>::size() const
{
    return size_helper(root);
}
template <typename K, typename V>
void MyMap<K, V>::clear()
{
    clear_helper(root);
    root = nullptr;
}
template <typename K, typename V>
void MyMap<K, V>::insert(const MyPair<K, V> &init_pair)
{
    insert_helper(root, init_pair);
}
template <typename K, typename V>
void MyMap<K, V>::erase(const K &key)
{
    erase_helper(root, key);
}
template <typename K, typename V>
MyPair<K, V> * MyMap<K, V>::find(const K &key) const
{
    return find_helper(root, key);
}
template <typename K, typename V>
void MyMap<K, V>::print() const
{
    print_helper(root, "");
}
template <typename K, typename V>
int MyMap<K, V>::count(const K &key) const
{
    return count_helper(root, key);
}
